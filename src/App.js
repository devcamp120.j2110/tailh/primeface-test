import "./App.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import { Divider } from "primereact/divider";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";

function App() {
  // State
  const [visible, setVisible] = useState(false);
  const toast = useRef(null);

  return (
    <main>
      <div className="main-content">
        {/* overlay */}
        <div className="overlay"></div>
        {/* card */}
        <div className="card">
          <div className="grid">
            <div className="col-5 flex align-items-center justify-content-center">
              <div className="p-fluid">
                <div className="field">
                  <label htmlFor="username">Username</label>
                  <InputText id="username" type="text" />
                </div>
                <div className="field">
                  <label htmlFor="password">Password</label>
                  <InputText id="password" type="password" />
                </div>
                <Button label="Login"></Button>
              </div>
            </div>
            <Divider layout="vertical">
              <div
                className="col-2"
                style={{
                  display: "flex",
                  margin: "auto",
                  textAlign: "center",
                  justifyContent: "center",
                }}
              >
                <b>OR</b>
              </div>
            </Divider>
            <div className="col-5 flex align-items-center justify-content-center">
              <Button
                label="Sign Up"
                icon="pi pi-user-plus"
                className="p-button-success"
              ></Button>
            </div>
          </div>
        </div>
        {/* Dialog */}
        {/* <ConfirmDialog
          visible={visible}
          onHide={() => setVisible(false)}
          message="Are you sure you want to proceed?"
          header="Confirmation"
          icon="pi pi-exclamation-triangle"
          accept={accept}
          reject={reject}
        /> */}
      </div>
    </main>
  );
}

export default App;
